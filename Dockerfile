FROM debian:bookworm
RUN apt-get update && apt-get install -qy auto-apt-proxy
RUN apt-get update && apt-get install -qy ruby puma ruby-sinatra ruby-haml ruby-rest-client ruby-json wget
COPY ./ /app
WORKDIR /app
USER www-data
CMD puma --bind tcp://0.0.0.0:${PORT:-5000}
