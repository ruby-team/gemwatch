# encoding: UTF-8

require 'sinatra'
require 'haml'
require 'restclient'
require 'json'

require 'gem2tgz'


require 'open-uri'


module GemWatch
  DOWNLOAD_BASE = ENV['DOWNLOAD_BASE'] || File.join(File.dirname(__FILE__), 'tmp', 'download')
  ASSETS_BASE = File.join(File.dirname(__FILE__), 'public')

  class << self
    attr_accessor :assets_path, :prefix
  end
  self.assets_path = ""
  self.prefix = ""
  def asset_url(url)
    GemWatch.assets_path + url + '?' + File.mtime(ASSETS_BASE + url).to_i.to_s
  end
  def app_url(url)
    GemWatch.prefix + url
  end
end

class GemWatch::Gem
  class NotFound < Exception; end
  class CommandFailed < Exception; end

  def self.get(gemname, forced_version = nil)
    if gemname =~ /^rails-assets/
      GemWatch::RailsAssetsGem.new(gemname, forced_version)
    else
      new(gemname, forced_version)
    end
  end

  def initialize(gemname, forced_version = nil)
    begin
      @data = JSON.parse(RestClient.get("http://rubygems.org/api/v1/gems/#{gemname}.json").body)
      if forced_version
        @data['version'] = forced_version
        @data['gem_uri'] =  File.dirname(@data['gem_uri']) + "/#{directory}.gem"
      end
    rescue RestClient::ResourceNotFound
      raise GemWatch::Gem::NotFound
    end
  end
  def name
    @data['name']
  end
  def version
    @data['version']
  end
  def uri
    @data['gem_uri']
  end
  def info
    @data['info']
  end
  def home
    @data['project_uri']
  end
  def authors
    @data['authors']
  end
  def gem
    File.basename(uri)
  end
  def run(*cmd)
    subprocess = IO.popen(cmd, :err => :out)
    output = subprocess.read
    subprocess.close
    if $? && $?.exitstatus > 0
      raise GemWatch::Gem::CommandFailed, "#{cmd} failed!\n" + output
    end
  end
  def download(uri)
    URI.open(uri, 'rb') do |downloaded_gem|
      File.open(gem, 'wb') do |file|
        file.write(downloaded_gem.read)
      end
    end
  end
  def download_and_convert!
    unless File.exist?(tarball_path)
      FileUtils.mkdir_p(download_dir)
      Dir.chdir(download_dir) do
        download uri
        Gem2Tgz.convert!(gem, tarball)
        FileUtils.rm_f(gem)
      end
    end
  end
  def directory
    "#{name}-#{version}"
  end
  def download_dir
    @download_dir ||= File.join(GemWatch::DOWNLOAD_BASE, name[0..0], name)
  end
  def tarball
    "#{directory}.tar.gz"
  end
  def tarball_path
    File.join(download_dir, tarball)
  end
  def archived
    @archived ||= Dir.glob(File.join(download_dir, '*.tar.gz')).map { |f| File.basename(f) }
  end
  def archived?(tarball)
    archived.include?(tarball)
  end
  def archived_path(tarball)
    File.join(download_dir, tarball)
  end
  def download_for(wanted_version)
    tarball = "#{name}-#{wanted_version}.tar.gz"
    if archived?(tarball)
      archived_path(tarball)
    else
      if wanted_version == version
        download_and_convert!
        tarball_path
      else
        raise GemWatch::Gem::NotFound
      end
    end
  end
end

class GemWatch::RailsAssetsGem < GemWatch::Gem
  def initialize(gemname, forced_version = nil)
    @data = {}
    @data['name'] = gemname
    @data['version'] = forced_version || latest_version
    @data['gem_uri'] = "https://rails-assets.org/gems/#{directory}.gem"
  end

  protected

  def latest_version
    cache.find do |entry|
      entry.first == name
    end[1].to_s
  end

  def cache
    cache_dir = GemWatch::DOWNLOAD_BASE
    cache_file = File.join(cache_dir, 'rails-assets.marshall')

    if File.exists?(cache_file)
      seconds_since_last_update = Time.now.to_i - File.stat(cache_file).mtime.to_i
      cache_timeout = 60*60 # 1 hour
      if seconds_since_last_update < cache_timeout
        return Marshal.load(File.read(cache_file))
      end
    end

    lock_file = cache_file + '.lock'
    File.open(lock_file, 'w') do |f|
      if f.flock(File::LOCK_EX | File::LOCK_NB)
        # download
        run('wget', '-q', '-O', cache_file + '.gz', 'https://rails-assets.org/latest_specs.4.8.gz')
        run('rm', '-f', cache_file)
        run('gunzip', cache_file + '.gz')
        run('touch', cache_file)
      else
        # another process already downloading, just wait
        f.flock(File::LOCK_SH)
      end
      f.flock(File::LOCK_UN)

      Marshal.load(File.read(cache_file))
    end
  end

end


module HostHelper
  def host_with_port
    if request.respond_to?(:host_with_port)
      request.host_with_port
    else
      request.host + ([80,443].include?(request.port) ? '' : (':' + request.port.to_s))
    end
  end
end


class GemWatch::App < Sinatra::Base
  helpers GemWatch, HostHelper

  get '/?' do
    expires 86400, :public  # 1 day
    if params[:gem]
      redirect app_url("/#{params[:gem]}")
    else
      haml :index
    end
  end

  get '/:gem' do
    expires 14400, :public # 4 hours
    begin
      @gem = GemWatch::Gem.get(params[:gem])
      haml :gem
    rescue GemWatch::Gem::NotFound
      not_found
    end
  end

  get '/:gem/tarball' do
    thegem = GemWatch::Gem.get(params[:gem])
    redirect app_url("/#{thegem.name}/#{thegem.tarball}")
  end

  get '/download/:tarball' do
    expires 86400000, :public # 1000 days, published versions are supposed to not change
    begin
      params[:tarball] =~ /^(.+)-(.+).tar.gz$/
      gem_name = $1
      gem_version = $2

      gem = GemWatch::Gem.get(gem_name, gem_version)
      send_file gem.download_for(gem_version)
    rescue GemWatch::Gem::NotFound
      not_found
    end
  end

  # legacy links from gemwatch as CGI on alioth
  get '/cgi-bin/gemwatch' do
    redirect '/'
  end
  get '/cgi-bin/gemwatch/:gem' do
    redirect "/#{params[:gem]}"
  end

  not_found do
    haml :not_found
  end

  error 500 do
    haml :error
  end
end
