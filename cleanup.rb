$LOAD_PATH << File.dirname(__FILE__)
require "gemwatch"
require 'pathname'

class Item < Struct.new(:file)
  def version
    @version ||=
      begin
	f = File.basename(file)
	f =~ /^(.+)-(.+).tar.gz$/
	Gem::Version.new($2)
      end
  end
end

dir = Pathname.new(GemWatch::DOWNLOAD_BASE)
dir.glob('*/*').each do |pkg|
  files = pkg.glob("*")
  tarballs = []
  files.each do |f|
    if f.directory?
      puts "I: removing #{f} (directory)"
      f.rmtree
    elsif f.basename.to_s =~ /\.gem$/
      puts "I: removing #{f}"
      f.unlink
    elsif f.basename.to_s =~ /\.tar\.gz$/
      tarballs << Item.new(f)
    else
      $stderr.puts "W: unhandled file #{f}"
    end
  end
  tarballs.sort_by!(&:version)
  (1..3).each do
    tarballs.pop unless tarballs.empty?
  end
  tarballs.each do |t|
    puts "I: removing #{t.file}"
    t.file.unlink
  end
end
