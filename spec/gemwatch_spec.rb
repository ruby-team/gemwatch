require 'rack/test'
require 'uri'

$: << File.dirname(__FILE__) + '/..'
require 'gemwatch'

describe 'gemwatch' do

  include Rack::Test::Methods

  def app
    GemWatch::App
  end

  class FakeResponse < Struct.new(:body)
  end

  before(:each) do
    body = JSON.dump({
      "name" => "did_you_mean",
      "info" => "yada yada yada",
      "project_uri" => "https://example.com/",
      "authors" => ["The Author"],
      "version" => '1.2.3',
      'gem_uri' => "https://foo.example.com/did_you_mean/did_you_mean-1.2.3.gem",
    })
    resp = FakeResponse.new(body)
    allow(RestClient).to receive(:get).with("http://rubygems.org/api/v1/gems/did_you_mean.json").and_return(resp)
  end

  it 'displays home page' do
    get('/')
    expect(last_response.status).to eq(200)
  end

  it 'lists downloads' do
    get('/did_you_mean')
    expect(last_response.body).to match(%r{/download/did_you_mean-(.*)\.tar\.gz})
  end

  it 'redirects legacy links' do
    get('/cgi-bin/gemwatch/did_you_mean')
    follow_redirect!
    uri = URI.parse(last_request.url)
    expect(uri.path).to eq('/did_you_mean')
  end

  it 'redirects legacy link to home' do
    get('/cgi-bin/gemwatch')
    follow_redirect!
    uri = URI.parse(last_request.url)
    expect(uri.path).to eq('/')
  end

  it 'redirects to latest tarball' do
    get '/did_you_mean/tarball'
    expect(last_response.status).to eq(302)
    expect(last_response.location).to match(%r{/did_you_mean-1.2.3.tar.gz$})
  end

end
